/**
 * Padding outputs 2 characters always
 * @param {string} rgb one or two characters
 * @returns {string} rgb with two characters
 */

 module.exports = {
     /**
      * converts Hex to RGB 
      * @param {number} Hex 
      * @returns {number} rgb 
      */
     Hextorgb: (hex) => {
         const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
         return result ? {
            r: parseInt(result[1], 16),
            g: parseInt(result[2], 16),
            b: parseInt(result[3], 16)
        } : null;
     }

    
 }