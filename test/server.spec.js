// Integration test

const expect = require("chai").expect;
const request = require("request");
const app = require("../src/server");
const port = 2000;

describe("Color code Converter API", () => {

    before("Start server before run tests", (done) => {
        server = app.listen(port, () => {
            console.log(`Server listening: localhost:${port}`);
            done();
        });
    });

    describe("Hex to RGB conversion", () => {
        const url = `http://localhost:${port}/hex-to-rgb?hex=33ffff`;

        it("returns the color in rgb", (done) => {
            request(url, (error, response, body) => {
                expect(body).to.equal({"r":51,"g":255,"b":255});
                done();
            });
        });
    });

    after("Stop server after tests", (done) => {
        server.close();
        done();
    })
});