// TDD - unit testing

const expect = require("chai").expect;
const converter = require("../src/converter");

describe("Color code Converter", () => {
    describe("Hex to RGB conversion", () => {
        it("converts the basic colors", () => {
            const redrgb = converter.Hextorgb("#330000").r; // (51,0,0)
            const greenrgb = converter.Hextorgb("#00ff00").g; // (0,255,0)
            const bluergb = converter.Hextorgb("#0000ff").b; // (0,0,255)

            expect(redrgb).to.equal(51);
            expect(greenrgb).to.equal(255);
            expect(bluergb).to.equal(255);
        });
    });
});